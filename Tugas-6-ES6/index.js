//soal  no 1 
const luas = (p, l) => { return p * l };
const keliling = (p,l)=>{return (2*p) + (2*l)}; 

let p = 10;
let l = 5;
console.log(luas(p,l));
console.log(keliling(p,l));

// soal no 2

const newFunction = (firstname, lastname) =>{
    return{
        firstname: firstname,
        lastname: lastname,
        fullName: () =>{
          console.log(firstname + " " + lastname)
        }
    }
}
//jawaban soal no 2
newFunction("wahyu","fajar").fullName();

//soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
//jawaban no 3
const {firstName,lastName,address,hobby} = newObject;
console.log(firstName, lastName, address, hobby);

//soal no 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
//jawaban no 4
const combined = west.concat(east)
console.log(combined);

// soal no 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet;
var after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,${planet}`;
console.log(after);
