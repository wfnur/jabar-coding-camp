//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();   // jawaban soal 1
for(var i =0;i <daftarHewan.length;i++){
    console.log(daftarHewan[i])
}

//soal 2
function  introduce(data){// jawaba soal no 2
    var str = "Nama saya "+data.name+", umur saya "+data.age+" tahun, Alamat saya di "+data.address+", dan saya punya hobby yaitu "+data.hobby;
    return str; 
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

//soal 3
function hitung_huruf_vokal(str){ // jawaban soal no 3
    var count = str.match(/[aeiou]/gi).length;
    return count;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2)

// soal 4
var awal = -2;
var add = 2;
var hasil = 0;
function hitung(bil){ // jawaban soal no 4
    for(var i = 0;i <= bil;i++){
        if(i==0){
            hasil = awal;
        }else{
            hasil = hasil+add;
        }
    }
    return hasil;
}
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));