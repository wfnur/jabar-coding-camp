//soal 1
var nilai = 100;
var index;
if(nilai >= 85){
    index ='A';
}else if(nilai >= 75 && nilai < 85){
    index ='B';
}else if(nilai >= 65 && nilai < 75){
    index ='C';
}else if(nilai >= 55 && nilai < 65){
    index ='D';
}else if(nilai < 55){
    index ='E';
}

console.log(index) // jawaban soal 1

//soal 2
var tanggal = 8;
var bulan = 9;
var tahun =1997;
var strBulan;
switch (bulan) {
    case 1:
        strBulan = 'Januari';
    break;
    case 2:
        strBulan = 'Februari';
    break;
    case 3:
        strBulan = 'Maret';
    break;
    case 4:
        strBulan = 'April';
    break;
    case 5:
        strBulan = 'Mei';
    break;
    case 6:
        strBulan = 'Juni';
    break;
    case 7:
        strBulan = 'Juli';
    break;
    case 8:
        strBulan = 'Agustus';
    break;
    case 9:
        strBulan = 'September';
    break;
    case 10:
        strBulan = 'Oktober';
    break;
    case 11:
        strBulan = 'November';
    break;
    case 12:
        strBulan = 'Desember';
    break;
    
    default:
        strBulan = "NOT FOUND";
    break;
}
console.log(tanggal +" "+strBulan+" "+tahun); // jawaban soal 2

//soal 3
var n =5;
var hasil='';
for(i=1;i<=n;i++){
    for(j=0;j<i;j++){
        hasil +='*';
    }
    hasil+='\n';
}
console.log(hasil) // jawaban soal no 3


//soal 4

var m =6;
for(var i=1;i<=m;i++){
    // jawaban soal 4
    switch (i%3) {
        case 1:
            console.log(i+" - I love programming");
        break;
        case 2:
            console.log(i+" - I love Javascript");
        break;
        case 0:
            console.log(i+" - I love VueJS");
        break;
    
        default:
            break;
    }
    if(i%3==0){
        console.log("===")
    }
}