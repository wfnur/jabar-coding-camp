//Soal 1 : Function Penghitung Jumlah Kata

function jumlah_kata(str){
    return str.split(" ").length;
}

var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok";
var kalimat_2 = "Saya Iqbal";

//jawaban soal no 1
console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));

//soal 2 : Function Penghasil Tanggal Hari Esok
function next_date(tanggal, bulan, tahun){
    //add date
    var currDate = new Date(tahun+"-"+bulan+"-"+tanggal);
    var nextDate = new Date(currDate);

    nextDate.setDate(currDate.getDate() + 1);

    //change format
    var tahun = nextDate.getFullYear();
    var bulan = nextDate.getMonth();
    var tanggal = nextDate.getDate();

    switch(bulan) {
        case 0: bulan = "Januari"; break;
        case 1: bulan = "Februari"; break;
        case 2: bulan = "Maret"; break;
        case 3: bulan = "April"; break;
        case 4: bulan = "Mei"; break;
        case 5: bulan = "Juni"; break;
        case 6: bulan = "Juli"; break;
        case 7: bulan = "Agustus"; break;
        case 8: bulan = "September"; break;
        case 9: bulan = "Oktober"; break;
        case 10: bulan = "November"; break;
        case 11: bulan = "Desember"; break;
    }

    var newStrDate = tanggal + " "+bulan+" "+tahun;
    console.log(newStrDate)
    return newStrDate;
}

var tanggal = 31;
var bulan = 12;
var tahun = 2020;
//jawaban soal no 1
console.log(next_date(tanggal, bulan,tahun));