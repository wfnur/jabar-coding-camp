// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var kata1 = pertama.substring(0,4);
var kata2 = pertama.substring(12,18);
var kata3 = kedua.substring(0,7);
var kata4 = kedua.substring(8,18).toUpperCase();
var kalimatBaru = kata1+" "+kata2+" "+kata3+" "+kata4
console.log(kalimatBaru); // jawaban soal 1

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var bil1 = parseInt(kataPertama);
var bil2 = parseInt(kataKedua);
var bil3 = parseInt(kataKetiga);
var bil4 = parseInt(kataKeempat);

var hasil = bil2*bil3+bil1+bil4;
console.log(hasil); // jawaban soal 1

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 15); 
var kataKetiga=  kalimat.substring(15, 18); 
var kataKeempat= kalimat.substring(19, 24);  
var kataKelima = kalimat.substring(25, 31); 

//jawaban soal 3
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
